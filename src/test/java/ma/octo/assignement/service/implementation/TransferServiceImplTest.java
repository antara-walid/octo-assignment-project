package ma.octo.assignement.service.implementation;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Transfer;
import ma.octo.assignement.dto.TransferDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.repository.TransferRepository;
import ma.octo.assignement.service.AuditService;
import ma.octo.assignement.service.CompteService;
import ma.octo.assignement.service.TransferService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;


@ExtendWith(MockitoExtension.class)
class TransferServiceImplTest {

    @Mock
    private TransferRepository transferRepository;
    @Mock
    private CompteService compteService;
    @Mock
    private AuditService auditService;
    private TransferService transferService;


    public static final BigDecimal MONTANT_MAXIMAL = BigDecimal.valueOf(10000);

    Transfer transfer;
    Compte compteBenificaire;
    Compte compteEmetteur;

    TransferDto transferDto;

    @BeforeEach
    void setUp() {
        transferService = new TransferServiceImpl(transferRepository, compteService, auditService);
        compteBenificaire = new Compte();
        compteBenificaire.setSolde(BigDecimal.valueOf(100000));
        compteBenificaire.setRib("test");
        compteBenificaire.setNrCompte("nr");
        compteEmetteur = new Compte();
        compteEmetteur.setSolde(BigDecimal.valueOf(100000));
        compteEmetteur.setRib("test");
        compteEmetteur.setNrCompte("nr");

        transfer = new Transfer();
        transfer.setIdTransfer(1l);
        transfer.setMontantTransfer(BigDecimal.valueOf(1000));
        transfer.setMotifTransfer("test");
        transfer.setCompteBeneficiaire(compteBenificaire);
        transfer.setCompteEmetteur(compteEmetteur);

        transferDto = new TransferDto();
        transferDto.setNrCompteBeneficiaire("tt");
        transferDto.setNrCompteEmetteur("test");
        transferDto.setMotif("motif");
    }



    @Test
    void shouldGetAllTransfers() {
        //when
        transferService.getAll();
        //then
        verify(transferRepository).findAll();
    }

    @Test
    void shouldExecuteTransfer() throws TransactionException, CompteNonExistantException, SoldeDisponibleInsuffisantException {
        transferDto.setMontant(BigDecimal.valueOf(1000));
        //given
        given(compteService.findByNrCompte(transferDto.getNrCompteEmetteur())).willReturn(Optional.of(compteEmetteur));
        given(compteService.findByNrCompte(transferDto.getNrCompteBeneficiaire())).willReturn(Optional.of(compteBenificaire));

        // when
        transferService.executerTransfer(transferDto);
        // then
        assertEquals(compteBenificaire.getSolde(), BigDecimal.valueOf(100000 + 1000));
        assertEquals(compteEmetteur.getSolde(), BigDecimal.valueOf(100000 - 1000));

        verify(compteService).updateCompte(compteBenificaire);
        verify(compteService).updateCompte(compteEmetteur);
        verify(auditService).auditTransfer(any());


    }

    @Test
    void shouldThrowCompteNonExistantException() {
        // given
        given(compteService.findByNrCompte(transferDto.getNrCompteEmetteur())).willReturn(Optional.empty());
        assertThrows(CompteNonExistantException.class, () -> transferService.executerTransfer(transferDto));
    }

    @Test
    void shouldThrowTransactionException() {
        // given
        transferDto.setMontant(BigDecimal.ZERO);
        given(compteService.findByNrCompte(transferDto.getNrCompteEmetteur())).willReturn(Optional.of(compteEmetteur));
        given(compteService.findByNrCompte(transferDto.getNrCompteBeneficiaire())).willReturn(Optional.of(compteBenificaire));

        TransactionException exception =assertThrows(TransactionException.class, () -> transferService.executerTransfer(transferDto));
        assertTrue(exception.getMessage().contains("Montant vide"));

    }

    @Test
    void shouldThrowSoldeDisponibleInsuffisantException()
    {
        //given
        given(compteService.findByNrCompte(transferDto.getNrCompteEmetteur())).willReturn(Optional.of(compteEmetteur));
        given(compteService.findByNrCompte(transferDto.getNrCompteBeneficiaire())).willReturn(Optional.of(compteBenificaire));
        compteEmetteur.setSolde(BigDecimal.valueOf(100));
        transferDto.setMontant(BigDecimal.valueOf(1000));
       assertThrows(SoldeDisponibleInsuffisantException.class, () -> transferService.executerTransfer(transferDto));

    }

    @Test
    void saveTransfer() {
    }
}