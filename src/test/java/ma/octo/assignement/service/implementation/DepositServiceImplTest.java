package ma.octo.assignement.service.implementation;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Deposit;
import ma.octo.assignement.dto.DepositDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.repository.DepositRepository;
import ma.octo.assignement.service.AuditService;
import ma.octo.assignement.service.CompteService;
import ma.octo.assignement.service.DepositService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

import static org.junit.jupiter.api.Assertions.*;


@ExtendWith(MockitoExtension.class)
class DepositServiceImplTest {

    @Mock
    private DepositRepository depositRepository;
    @Mock
    private CompteService compteService;
    @Mock
    private AuditService auditService;
    private DepositService depositService;

    @BeforeEach
    void setUp() {
        depositService = new DepositServiceImpl(depositRepository, compteService, auditService);
    }

    @Test
    void shouldExecutedeposit() throws CompteNonExistantException {
        // given
        Compte compteBenificiare = new Compte();
        compteBenificiare.setRib("rib");
        compteBenificiare.setSolde(BigDecimal.valueOf(10000));

        given(compteService.findByRIB(any())).willReturn(Optional.of(compteBenificiare));
        given(Optional.of(compteBenificiare).isPresent());

        //when
        DepositDto depositDto = new DepositDto();
        depositDto.setMontant(BigDecimal.valueOf(1000));
        depositDto.setRIB("rib");
        depositDto.setMotifDeposit("test");

        depositService.executerdeposit(depositDto);

        //then
        assertEquals(compteBenificiare.getSolde(), BigDecimal.valueOf(11000));
        verify(compteService).updateCompte(compteBenificiare);
        verify(depositRepository).save(any());
        verify(auditService).auditDeposit(any());


    }

    @Test
    void ShouldgetAllDeposits() {
        //when
        depositService.getAll();
        //then
        verify(depositRepository).findAll();
    }

    @Test
    void shouldThrowCompteNonExistantException() throws CompteNonExistantException {
        //given
        Optional<Compte> compteOptional = Optional.empty();
        DepositDto depositDto = new DepositDto();
        depositDto.setRIB("rib");
        given(compteService.findByRIB(depositDto.getRIB())).willReturn(compteOptional);

        // when // then
        assertThrows(CompteNonExistantException.class, () -> depositService.executerdeposit(depositDto));
    }
}