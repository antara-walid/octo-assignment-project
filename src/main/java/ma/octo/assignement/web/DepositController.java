package ma.octo.assignement.web;


import ma.octo.assignement.dto.DepositDto;
import ma.octo.assignement.dto.TransferDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.service.DepositService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/deposits")
public class DepositController {

    private final DepositService depositService;

    public DepositController(DepositService depositService) {
        this.depositService = depositService;
    }

    @GetMapping
    List<DepositDto> getAll() {
        return depositService.getAll();
    }


    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void executerDeposit(@RequestBody DepositDto depositDto) throws CompteNonExistantException {
        depositService.executerdeposit(depositDto);
    }


}
