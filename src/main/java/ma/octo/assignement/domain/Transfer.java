package ma.octo.assignement.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import ma.octo.assignement.dto.TransferDto;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "TRANSFER")
@Builder
@AllArgsConstructor
public class Transfer {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long idTransfer;

    @Column(precision = 16, scale = 2, nullable = false)
    private BigDecimal montantTransfer;

    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateExecution;



    @ManyToOne
    private Compte compteEmetteur;

    @ManyToOne
    private Compte compteBeneficiaire;

    @Column(length = 200)
    private String motifTransfer;


    public Transfer() {
    }

    public BigDecimal getMontantTransfer() {
        return montantTransfer;
    }

    public void setMontantTransfer(BigDecimal montantTransfer) {
        this.montantTransfer = montantTransfer;
    }

    public Date getDateExecution() {
        return dateExecution;
    }

    public void setDateExecution(Date dateExecution) {
        this.dateExecution = dateExecution;
    }

    public Compte getCompteEmetteur() {
        return compteEmetteur;
    }

    public void setCompteEmetteur(Compte compteEmetteur) {
        this.compteEmetteur = compteEmetteur;
    }

    public Compte getCompteBeneficiaire() {
        return compteBeneficiaire;
    }

    public void setCompteBeneficiaire(Compte compteBeneficiaire) {
        this.compteBeneficiaire = compteBeneficiaire;
    }

    public String getMotifTransfer() {
        return motifTransfer;
    }

    public void setMotifTransfer(String motifTransfer) {
        this.motifTransfer = motifTransfer;
    }

    public Long getIdTransfer() {
        return idTransfer;
    }

    public void setIdTransfer(Long id) {
        this.idTransfer = id;
    }

    public TransferDto convertToDTO() {
        TransferDto transferDto = new TransferDto();
        transferDto.setNrCompteEmetteur(this.compteEmetteur.getNrCompte());
        transferDto.setNrCompteBeneficiaire(this.compteBeneficiaire.getNrCompte());
        transferDto.setMotif(this.motifTransfer);
        transferDto.setMontant(this.montantTransfer);
        transferDto.setDate(this.dateExecution);

        return transferDto;
    }
}
