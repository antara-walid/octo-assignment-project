package ma.octo.assignement.domain;

import ma.octo.assignement.domain.util.EventType;

import javax.persistence.*;

@Entity
@Table(name = "AUDIT_TRANSFER")
public class AuditTransfer {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long idAuditTransfer;

  @Column(length = 100)
  private String message;

  @Enumerated(EnumType.STRING)
  private EventType eventType;

  public Long getIdAuditTransfer() {
    return idAuditTransfer;
  }

  public void setIdAuditTransfer(Long id) {
    this.idAuditTransfer = id;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public EventType getEventType() {
    return eventType;
  }

  public void setEventType(EventType eventType) {
    this.eventType = eventType;
  }
}
