package ma.octo.assignement.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import ma.octo.assignement.dto.DepositDto;


import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "MONEY_DEPOSIT")
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Deposit {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long idMoneyDeposit;

  @Column(precision = 16, scale = 2, nullable = false)
  private BigDecimal montant;

  @Column
  @Temporal(TemporalType.TIMESTAMP)
  private Date dateExecution;

  @Column
  private String nomPrenomEmetteur;

  @ManyToOne
  @JoinColumn(name = "CompteBeneficiaireId")
  private Compte compteBeneficiaire;

  @Column(length = 200)
  private String motifDeposit;

  public BigDecimal getMontant() {
    return montant;
  }

  public void setMontant(BigDecimal montant) {
    this.montant = montant;
  }

  public Date getDateExecution() {
    return dateExecution;
  }

  public void setDateExecution(Date dateExecution) {
    this.dateExecution = dateExecution;
  }

  public Compte getCompteBeneficiaire() {
    return compteBeneficiaire;
  }

  public void setCompteBeneficiaire(Compte compteBeneficiaire) {
    this.compteBeneficiaire = compteBeneficiaire;
  }

  public String getMotifDeposit() {
    return motifDeposit;
  }

  public void setMotifDeposit(String motifDeposit) {
    this.motifDeposit = motifDeposit;
  }

  public Long getIdMoneyDeposit() {
    return idMoneyDeposit;
  }

  public void setIdMoneyDeposit(Long id) {
    this.idMoneyDeposit = id;
  }

  public String getNomPrenomEmetteur() {
    return nomPrenomEmetteur;
  }

  public void setNomPrenomEmetteur(String nomPrenomEmetteur) {
    this.nomPrenomEmetteur = nomPrenomEmetteur;
  }

  public DepositDto convertToDTO() {
    DepositDto depositDto = DepositDto.builder()
            .RIB(this.compteBeneficiaire.getRib())
            .motifDeposit(this.motifDeposit)
            .dateExecution(this.dateExecution)
            .montant(this.montant)
            .nomPrenomEmetteur(this.nomPrenomEmetteur)
            .build();


    return depositDto;
  }
}
