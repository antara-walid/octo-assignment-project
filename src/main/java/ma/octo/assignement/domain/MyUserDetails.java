package ma.octo.assignement.domain;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class MyUserDetails implements UserDetails {
    private String userName;
    private String password;


    List<GrantedAuthority> authorities= new ArrayList<>();


    public MyUserDetails(Utilisateur utilisateur) {
        this.userName = utilisateur.getUsername();
        this.password = utilisateur.getPassword();
        utilisateur.getRoles().forEach((r) -> authorities.add(new SimpleGrantedAuthority(r.getName())));
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return userName;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true; // always return true to simplify
    }
}
