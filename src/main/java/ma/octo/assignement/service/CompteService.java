package ma.octo.assignement.service;

import ma.octo.assignement.domain.Compte;

import java.util.List;
import java.util.Optional;

public interface CompteService {

    Optional<Compte> findByNrCompte(String nrCompte);
    Compte updateCompte(Compte compte);
    List<Compte> getAll();

    Optional<Compte> findByRIB(String RIB);
}
