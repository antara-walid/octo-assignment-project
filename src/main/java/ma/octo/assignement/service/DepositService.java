package ma.octo.assignement.service;

import ma.octo.assignement.dto.DepositDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;

import java.util.List;

public interface DepositService {

    void executerdeposit(DepositDto depositDto) throws CompteNonExistantException;

    List<DepositDto> getAll();
}
