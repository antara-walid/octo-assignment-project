package ma.octo.assignement.service;

import ma.octo.assignement.domain.Utilisateur;

import java.util.List;
import java.util.Optional;

public interface UtilisateurService {
    List<Utilisateur> getAll();
    Optional<Utilisateur> findByUserName(String username);
}
