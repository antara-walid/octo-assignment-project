package ma.octo.assignement.service.implementation;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Deposit;
import ma.octo.assignement.dto.DepositDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.repository.DepositRepository;
import ma.octo.assignement.service.AuditService;
import ma.octo.assignement.service.CompteService;
import ma.octo.assignement.service.DepositService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class DepositServiceImpl implements DepositService {

    private final DepositRepository depositRepository;
    private final CompteService compteService;

    private final AuditService auditService;

    public DepositServiceImpl(DepositRepository depositRepository, CompteService compteService, AuditService auditService) {
        this.depositRepository = depositRepository;
        this.compteService = compteService;
        this.auditService = auditService;
    }

    @Override
    public void executerdeposit(DepositDto depositDto) throws CompteNonExistantException {
        // 1. get Account by RIB
        Compte compteBenificiare = compteService.findByRIB(depositDto.getRIB())
                .orElseThrow(() -> new CompteNonExistantException("Compte Non existant"));
        //2. change balance
        compteBenificiare.setSolde(compteBenificiare.getSolde().add(depositDto.getMontant()));

        //3. saving
        compteService.updateCompte(compteBenificiare);

        Deposit deposit = Deposit.builder()
                .nomPrenomEmetteur(depositDto.getNomPrenomEmetteur())
                .motifDeposit(depositDto.getMotifDeposit())
                .montant(depositDto.getMontant())
                .dateExecution(depositDto.getDateExecution())
                .compteBeneficiaire(compteBenificiare)
                .build();
        depositRepository.save(deposit);

        auditService.auditDeposit("deposit " + "d'un montant de " + depositDto.getMontant()+" vers " + depositDto.getRIB());

    }

    @Override
    public List<DepositDto> getAll() {
        List<DepositDto> depositDtoList = new ArrayList<>();
        depositRepository.findAll().forEach(deposit -> depositDtoList.add(deposit.convertToDTO()));
        return depositDtoList;
    }
}
