package ma.octo.assignement.service.implementation;


import lombok.extern.slf4j.Slf4j;
import ma.octo.assignement.domain.MyUserDetails;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.service.UtilisateurService;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@Slf4j
public class UserDetailServiceImpl implements UserDetailsService {

    private final UtilisateurService utilisateurService;

    public UserDetailServiceImpl(UtilisateurService utilisateurService) {
        this.utilisateurService = utilisateurService;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<Utilisateur> utilisateurOptional = utilisateurService.findByUserName(username);
        log.info(utilisateurOptional.get().toString());
        utilisateurOptional.orElseThrow(() -> new UsernameNotFoundException("l'utilisateur " + username + "est introuvable"));
        return new MyUserDetails(utilisateurOptional.get());
    }
}
