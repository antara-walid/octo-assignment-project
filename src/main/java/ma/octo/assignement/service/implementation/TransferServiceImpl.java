package ma.octo.assignement.service.implementation;

import lombok.extern.slf4j.Slf4j;
import ma.octo.assignement.commun.utils.Constants;
import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Transfer;
import ma.octo.assignement.dto.TransferDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.repository.TransferRepository;
import ma.octo.assignement.service.AuditService;
import ma.octo.assignement.service.CompteService;
import ma.octo.assignement.service.TransferService;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;


@Service
@Transactional
@Slf4j
public class TransferServiceImpl implements TransferService {

    private final TransferRepository transferRepository;
    private final CompteService compteService;
    private final AuditService auditService;

    public static final BigDecimal MONTANT_MAXIMAL = BigDecimal.valueOf(10000);

    public TransferServiceImpl(TransferRepository transferRepository, CompteService compteService, AuditService auditService) {
        this.transferRepository = transferRepository;
        this.compteService = compteService;
        this.auditService = auditService;
    }

    @Override
    public List<TransferDto> getAll() {
        // first getting the lists
        List<TransferDto> transferListDTO = new ArrayList<>();
            transferRepository.findAll().forEach(transfer -> transferListDTO.add(transfer.convertToDTO()));
        return transferListDTO;
    }

    @Override
    public void executerTransfer(TransferDto transferDto) throws CompteNonExistantException, TransactionException, SoldeDisponibleInsuffisantException {

        //Emmetteur
        Compte compteEmmetteur = compteService.findByNrCompte(transferDto.getNrCompteEmetteur()).orElseThrow(() -> new CompteNonExistantException("Compte Non existant"));
        // Beneficiaire
        Compte compteBeneficiaire = compteService.findByNrCompte(transferDto.getNrCompteBeneficiaire()).orElseThrow(() -> new CompteNonExistantException("Compte Non existant"));
        // montant
        BigDecimal montant = transferDto.getMontant();


        // 2. check if the amount is valid
        if (montant == null || montant.equals(BigDecimal.ZERO)) {
            log.error("Montant vide");
            throw new TransactionException("Montant vide");
        } else if (montant.compareTo(Constants.MONTANT_MINIMAL) < 0) {
            log.error("Montant minimal de transfer non atteint");
            throw new TransactionException("Montant minimal de transfer non atteint");
        } else if (montant.compareTo(Constants.MONTANT_MAXIMAL) > 0) {
            log.error("Montant maximal de transfer dépassé");
            throw new TransactionException("Montant maximal de transfer dépassé");
        }

        // 3.check motif
        if (transferDto.getMotif().length() == 0) {
            log.error("Motif vide");
            throw new TransactionException("Motif vide");
        }

        // 4.check solde
        if (compteEmmetteur.getSolde().compareTo(transferDto.getMontant()) < 0) {
            log.error("solde insuffisant");
            throw new SoldeDisponibleInsuffisantException("solde insuffisant");
        }
        // 5. executing the transfer
        compteBeneficiaire.setSolde(compteBeneficiaire.getSolde().add(montant));
        compteEmmetteur.setSolde(compteEmmetteur.getSolde().subtract(montant));

        // 6. saving the account
        compteService.updateCompte(compteBeneficiaire);
        compteService.updateCompte(compteEmmetteur);

        // 7.saving the transfer
        Transfer transfer = Transfer.builder()
                .dateExecution(transferDto.getDate())
                .compteBeneficiaire(compteBeneficiaire)
                .compteEmetteur(compteEmmetteur)
                .montantTransfer(montant)
                .motifTransfer(transferDto.getMotif())
                .build();
        saveTransfer(transfer);

        auditService.auditTransfer("Transfer depuis " + transferDto.getNrCompteEmetteur() + " vers " + transferDto
                .getNrCompteBeneficiaire() + " d'un montant de " + transferDto.getMontant()
                .toString());


    }

    @Override
    public void saveTransfer(Transfer transfer) {
        transferRepository.save(transfer);
    }
}
