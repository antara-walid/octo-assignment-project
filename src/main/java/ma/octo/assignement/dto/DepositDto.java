package ma.octo.assignement.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ma.octo.assignement.domain.Deposit;

import java.math.BigDecimal;
import java.util.Date;


@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class DepositDto {

    private String RIB;
    private BigDecimal montant;
    private Date dateExecution;
    private String nomPrenomEmetteur;
    private String motifDeposit;


}
