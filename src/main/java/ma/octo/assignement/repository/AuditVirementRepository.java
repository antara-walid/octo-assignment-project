package ma.octo.assignement.repository;

import ma.octo.assignement.domain.Deposit;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AuditVirementRepository extends JpaRepository<Deposit, Long> {

}