package ma.octo.assignement.commun.utils;

import java.math.BigDecimal;

public class Constants {
    public static final BigDecimal MONTANT_MAXIMAL = BigDecimal.valueOf(10000);
    public static final BigDecimal MONTANT_MINIMAL = BigDecimal.valueOf(10);
}
