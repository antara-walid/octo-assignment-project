package ma.octo.assignement;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Role;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.domain.Transfer;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.RoleRepository;
import ma.octo.assignement.repository.UtilisateurRepository;
import ma.octo.assignement.repository.TransferRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@SpringBootApplication
public class NiceBankApplication implements CommandLineRunner {

    private final CompteRepository compteRepository;

    private final UtilisateurRepository utilisateurRepository;

    private final TransferRepository transferRepository;

    private final RoleRepository roleRepository;


    public NiceBankApplication(CompteRepository compteRepository, UtilisateurRepository utilisateurRepository, TransferRepository transferRepository, RoleRepository roleRepository) {
        this.compteRepository = compteRepository;
        this.utilisateurRepository = utilisateurRepository;
        this.transferRepository = transferRepository;
        this.roleRepository = roleRepository;
    }

    public static void main(String[] args) {
        SpringApplication.run(NiceBankApplication.class, args);
    }

    @Override
    public void run(String... strings) {
        // roles
        Role admin = new Role("ROLE_ADMIN");
        Role user = new Role("ROLE_USER");
        roleRepository.save(admin);
        roleRepository.save(user);

        Utilisateur utilisateur1 = new Utilisateur();
        utilisateur1.setUsername("user1");
        utilisateur1.setLastname("last1");
        utilisateur1.setFirstname("first1");
        utilisateur1.setGender("Male");
        utilisateur1.setPassword("user");
        utilisateur1.setRoles(List.of(user));

        utilisateurRepository.save(utilisateur1);


        Utilisateur utilisateur2 = new Utilisateur();
        utilisateur2.setUsername("user2");
        utilisateur2.setLastname("last2");
        utilisateur2.setFirstname("first2");
        utilisateur2.setGender("Female");
        utilisateur2.setPassword("user");
        utilisateur2.setRoles(List.of(user));

        utilisateurRepository.save(utilisateur2);

        // admin

        Utilisateur adminutilisateur = new Utilisateur();
        adminutilisateur.setUsername("admin");
        adminutilisateur.setLastname("admin");
        adminutilisateur.setFirstname("admin");
        adminutilisateur.setGender("Male");
        adminutilisateur.setPassword("admin");
        adminutilisateur.setRoles(List.of(admin));

        utilisateurRepository.save(adminutilisateur);

        Compte compte1 = new Compte();
        compte1.setNrCompte("010000A000001000");
        compte1.setRib("RIB1");
        compte1.setSolde(BigDecimal.valueOf(200000L));
        compte1.setUtilisateur(utilisateur1);

        compteRepository.save(compte1);

        Compte compte2 = new Compte();
        compte2.setNrCompte("010000B025001000");
        compte2.setRib("RIB2");
        compte2.setSolde(BigDecimal.valueOf(140000L));
        compte2.setUtilisateur(utilisateur2);

        compteRepository.save(compte2);

        Transfer v = Transfer.builder().build();
        v.setMontantTransfer(BigDecimal.TEN);
        v.setCompteBeneficiaire(compte2);
        v.setCompteEmetteur(compte1);
        v.setDateExecution(new Date());
        v.setMotifTransfer("Assignment 2021");

        transferRepository.save(v);
    }

}
